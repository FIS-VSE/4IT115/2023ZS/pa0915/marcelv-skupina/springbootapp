package cz.vse.weatherapp.controller;

import cz.vse.weatherapp.domain.jpa.AppUser;
import cz.vse.weatherapp.domain.jpa.Destination;
import cz.vse.weatherapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/destination")
public class DestinationController {

    @Autowired
    private UserService userService;

    @RequestMapping("/add")
    public String add(@RequestParam String userName, @RequestParam(required = false) String cityName, Model model) {
        AppUser user = userService.addDestination(userName, cityName);
        model.addAttribute("user", user);
        return "destination/add";
    }

    @RequestMapping("/list")
    public String list(@RequestParam String userName, Model model) {
        AppUser user = userService.getOrAddUser(userName);
        model.addAttribute("user", user);
        return "destination/list";
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String remove(@RequestParam String userName, @RequestParam String cityName, Model model) {
        userService.removeDestination(userName, cityName);
        model.addAttribute("user", userService.getOrAddUser(userName));
        return "destination/add";
    }

    @GetMapping("/sort")
    public String showSortedDestinations(@RequestParam String userName, Model model) {
        AppUser user = userService.getOrAddUser(userName); // Get the user
        List<Destination> destinations = user.getDestinations();
        destinations.sort(Comparator.comparing(Destination::getWeather).reversed());
        user.setDestinations(destinations);
        model.addAttribute("user", user);
        return "destination/add"; // return the view name
    }

}
