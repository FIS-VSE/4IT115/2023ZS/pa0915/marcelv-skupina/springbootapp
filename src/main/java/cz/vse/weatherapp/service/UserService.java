package cz.vse.weatherapp.service;

import cz.vse.weatherapp.domain.jpa.AppUser;
import cz.vse.weatherapp.domain.jpa.Destination;
import cz.vse.weatherapp.domain.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WeatherService weatherService;

    public AppUser getOrAddUser(String name) {
        AppUser user = userRepository.findByUserName(name);
        if (user == null) {
            user = createWithInitialDestinations(name);
            userRepository.save(user);
        }
        return user;
    }

    public AppUser createWithInitialDestinations(String userName) {
        AppUser user = new AppUser();
        user.setUserName(userName);

        // Add initial destinations
        Destination destination1 = new Destination();
        destination1.setName("Prague");
        destination1.setWeather(weatherService.getCurrentWeather("Prague"));

        Destination destination2 = new Destination();
        destination2.setName("Berlin");
        destination2.setWeather(weatherService.getCurrentWeather("Berlin"));

        Destination destination3 = new Destination();
        destination3.setName("Paris");
        destination3.setWeather(weatherService.getCurrentWeather("Paris"));

        List<Destination> destinations = user.getDestinations();
        if (destinations == null) {
            destinations = new ArrayList<>();
            destinations.addAll(Arrays.asList(destination1, destination2, destination3));
            user.setDestinations(destinations);
        } else {
            destinations.addAll(Arrays.asList(destination1, destination2, destination3));
        }

        userRepository.save(user);
        return user;
    }

    public AppUser addDestination(String userName, String cityName) {
        AppUser user = userRepository.findByUserName(userName);
        if (user != null && cityName != null) {
            Destination destination = new Destination();
            destination.setName(cityName);
            destination.setWeather(weatherService.getCurrentWeather(cityName));
            user.getDestinations().add(destination);
            userRepository.save(user);
        }
        return user;
    }

    public void removeDestination(String userName, String cityName) {
        AppUser user = userRepository.findByUserName(userName);
        if (user != null) {
            List<Destination> destinations = user.getDestinations();
            destinations.removeIf(destination -> destination.getName().equals(cityName));
            userRepository.save(user);
        }
    }


}
