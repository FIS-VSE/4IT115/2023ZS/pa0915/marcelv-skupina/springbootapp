package cz.vse.weatherapp.service;

import lombok.Getter;
import org.springframework.stereotype.Service;

@Service
@Getter
public class CountingService {

    private int pocet = 0;

    public void pricti() {
        pocet++;
    }

}
